package com.henrique.bookstore.services.exceptions;

import java.io.Serial;

public class DataIntegrityViolationException extends RuntimeException{

    @Serial
    private static final long serialVersionUID = -3681869042028616478L;

    public DataIntegrityViolationException(String message) {
        super(message);
    }

    public DataIntegrityViolationException(String message, Throwable cause) {
        super(message, cause);
    }
}
