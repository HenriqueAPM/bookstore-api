package com.henrique.bookstore.services;

import com.henrique.bookstore.domain.Categoria;
import com.henrique.bookstore.dtos.CategoriaDTO;
import com.henrique.bookstore.repositories.CategoriaRepository;
import com.henrique.bookstore.services.exceptions.DataIntegrityViolationException;
import com.henrique.bookstore.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CategoriaService {
    @Autowired
    private CategoriaRepository categoriaRepository;

    public Categoria findById(Integer id) {
        Optional<Categoria> obj = categoriaRepository.findById(id);
        return obj.orElseThrow(() -> new ObjectNotFoundException("Objeto não encontrado! ID:" + id + ", Tipo: " + Categoria.class.getName()));
    }

    public List<Categoria> findAll() {
        return categoriaRepository.findAll();
    }

    public Categoria create(Categoria obj) {
        obj.setId(null);
        return categoriaRepository.save(obj);
    }

    public Categoria update(Integer id, CategoriaDTO objDTO) {
        Categoria obj = findById(id);
        obj.setNome(objDTO.getNome());
        obj.setDescrição(objDTO.getDescrição());
        return categoriaRepository.save(obj);
    }

    public void delete(Integer id) {
        findById(id);
        try {
            categoriaRepository.deleteById(id);
        } catch (org.springframework.dao.DataIntegrityViolationException e) {
            throw new com.henrique.bookstore.services.exceptions.DataIntegrityViolationException(
                    "Categoria não pode ser deletada! Possui livros associados");
        }

    }
}
